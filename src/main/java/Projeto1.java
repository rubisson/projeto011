
import utfpr.ct.dainf.if62c.projeto.Poligonal;
import utfpr.ct.dainf.if62c.projeto.PontoXY;
import utfpr.ct.dainf.if62c.projeto.PontoXZ;
import utfpr.ct.dainf.if62c.projeto.PontoYZ;



/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Projeto1 {

    public static void main(String[] args) {
        PontoXY obj1 = new PontoXY(0,2);
        PontoXY obj2 = new PontoXY(0,1);
        PontoXY obj3 = new PontoXY(0,3);
        PontoXY[] vertices = new PontoXY[5];
        vertices[0] = obj1;
        vertices[1] = obj2;
        vertices[2] = obj3;
        vertices[3] = obj2;
        vertices[4] = obj3;
        Poligonal<PontoXY> objt = new Poligonal<>(vertices);
        String str="aaa";
  
        System.out.println(vertices[0]);
        System.out.println(objt.getN());
  
//        System.out.println(objt.getComprimento());
        System.out.println("nome não qualificado da classe >>> " + obj1.getNome());
        System.out.println("nome não qualificado da classe >>> " + obj2.getNome());
        System.out.println("nome não qualificado da classe >>> " + obj3.getNome());
    
        if (obj1.equals(obj2)) {
         System.out.println("distância entre dois pontos " + obj2.dist(obj1));
        }
        System.out.println("Chamando o getComprimento");
        objt.getComprimento();
        System.out.println(obj1.getX());
        System.out.println(objt.get(2));
         System.out.println("distância entre dois pontos " + obj2.dist(obj1));
        
        
    }
    
}
