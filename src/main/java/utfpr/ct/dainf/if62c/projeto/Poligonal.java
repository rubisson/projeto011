/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.projeto;

/**
 *
 * @author Rubinho
 * @param <T>
 */
public class Poligonal<T> extends Ponto{
    private T[] vertices;
    private int n;
    int i = 1;
    public Poligonal() {
    }
    
    public Poligonal(T[] vertices) {
        //gerar uma exceção não verificada RuntimeException
        //MSG_POLIG = "Poligonal deve ter ao menos 2 vértices;
        n=vertices.length;
        if (n<3){
            throw new RuntimeException("Poligonal deve ter ao menos 2 vértices");
        }else{
            this.vertices = vertices;
        }
    }

    public int getN() {
        return n;
    }

    public T get(int a){
        if ((a>=0)&&(a<n)){
           return vertices[a];
        }else{
            return null;
        }
    }
    
    public void set(int a, T pto){
        if ((a>=0)&&(a<n)){
           vertices[a]=pto;
        }
    }
    
    public double getComprimento(){
        double compr=0.0;
        for (T vertice:vertices) {
        System.out.println(vertice);
        }
        vertices[i].dist(vertices[i+1]);
  // Corrigir >>> errado;
        return compr;
    }
    
}
